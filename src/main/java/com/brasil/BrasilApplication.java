package com.brasil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class BrasilApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrasilApplication.class, args);
	}
	
	@RequestMapping("/oibrasil")
	public String meuBrasil() {
		return "Oi meu Brasil amado";
	}

}
